import React, { useState, useEffect } from "react";
import "./Navbar.css";

export default function Navbar() {
  const [toggleMenu, setToggleMenu] = useState(false);
  const [dimension, setDimension] = useState(window.innerWidth);

  const afficheMenu = () => {
    setToggleMenu(!toggleMenu);
  };

  useEffect(() => {
    function changeWidth() {
      setDimension(window.innerWidth);
    }

    window.addEventListener("resize", changeWidth);

    return () => window.removeEventListener("resize", changeWidth);
  }, []);

  return (
    <nav>
      {(dimension > 500 || toggleMenu) && (
        <ul className="liste">
          <li className="items">Accueil</li>
          <li className="items">Services</li>
          <li className="items">Contact</li>
        </ul>
      )}
      <button onClick={afficheMenu} className="btn">
        Menu
      </button>
    </nav>
  );
}
