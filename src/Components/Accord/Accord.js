import React, {useState, useRef, useEffect} from 'react'
import "./Accord.css";
import Chevron from "./arrow.svg"

export default function Accord() {

    const [toggle, setToggle] = useState(false);
    const [heightEl, setHeightEl] = useState();


    const toggleState = () => {
        setToggle(!toggle);
    }

    const refHeight = useRef();

    useEffect(() => {
        setHeightEl(`${refHeight.current.scrollHeight}px`);
    }, []);

    


    return (
        <div className="accord">

            <div className="accord-visible" onClick={toggleState}>

                <h2 className="accord-title">Lorem ipsum dolor</h2>
                <img src={Chevron} alt="arrow down" className="arrow" />

            </div>

            <div ref={refHeight} className={toggle ? "accord-toggle animated" : "accord-toggle"}
            style={{height: toggle ? `${heightEl}` : `0px`}}>

                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel quasi cum neque placeat possimus ratione deleniti impedit magni harum velit aut voluptatibus, laborum eaque. Temporibus ea fuga dolorum explicabo reiciendis.
                Iusto, molestias magni asperiores molestiae est ab enim. Fuga sunt pariatur expedita minima, id dolorum nam facere quam est voluptatibus distinctio quod iste repellendus aspernatur beatae nemo nostrum nulla. Fuga?
                Exercitationem impedit eum eligendi quasi. Amet mollitia debitis molestias reiciendis facilis ipsa adipisci commodi quos id, vitae aliquid vel accusamus repellat, iure provident aliquam dolores quia exercitationem natus quidem corporis.</p>

            </div>
            
        </div>
    )
}
